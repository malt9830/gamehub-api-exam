import axios, { AxiosInstance } from 'axios'
import 'dotenv/config'

/**
 * Axios HTTP client configured with a base URL from the TESTING_URI environment variable.
 * @type {import('axios').AxiosInstance}
 */
const apiClient: AxiosInstance = axios.create({
  baseURL: process.env.TESTING_URI,
})

export default apiClient
