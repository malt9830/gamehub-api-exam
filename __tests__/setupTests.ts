import mongoose from 'mongoose'
import 'dotenv/config'
import { Game, Genre, Platform } from '../models'
import { games, genres, platforms } from '../data'

/**
 * Sets up global test environment by connectin to MongoDB and seeding sample data.
 * @returns {Promis<void>}
 */
beforeAll(async () => {
  await mongoose.connect(process.env.TESTING_DB_URI as string)
  await Game.insertMany(games)
  await Genre.insertMany(genres)
  await Platform.insertMany(platforms)
})

/**
 * Tears down MongoDB by dropping the database and closing the connection.
 * @returns {Promis<void>}
 */
afterAll(async () => {
  await mongoose.connection.db.dropDatabase()
  await mongoose.connection.close()
})
