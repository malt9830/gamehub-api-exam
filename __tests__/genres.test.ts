import { AxiosResponse } from 'axios'
import { Genre, IGenre } from '../models'
import { genres } from '../data'
import apiClient from './apiClient'

interface GenresResponse extends AxiosResponse {
  data: {
    count: number
    results: IGenre[]
  }
}

interface GenreByIdResponse extends AxiosResponse {
  data: IGenre
}

describe('/genres', () => {
  let res: GenresResponse

  beforeAll(async () => {
    res = await apiClient.get('/genres')
  })

  it('should return the genres from the seeding', async () => {
    expect(res.status).toBe(200)
    expect(res.data.count).toBe(genres.length)
  })

  it('should return an array', async () => {
    expect(res.status).toBe(200)
    expect(Array.isArray(res.data.results)).toBe(true)
  })
})

describe('/genres when error', () => {
  it('should return status code 500 due to database error', async () => {
    jest.spyOn(Genre, 'find').mockRejectedValue(new Error('Database error'))
    try {
      await apiClient.get('/genres')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})

describe('/genres/:id when a genre is found', () => {
  let res: GenreByIdResponse

  beforeAll(async () => {
    res = await apiClient.get(`/genres/${genres[0]._id}`)
  })

  it('should return the requested genre', async () => {
    expect(res.status).toBe(200)
    expect(res.data._id).toBe(genres[0]._id)
  })
})

describe('/genres/:id when no genre is found', () => {
  it('should return status code 404', async () => {
    try {
      await apiClient.get('/genres/65ea65ea65ea65ea65ea65ea')
    } catch (err: any) {
      expect(err.response.status).toBe(404)
    }
  })
})

describe('/genres/:id when error', () => {
  it('should return status code 500', async () => {
    try {
      await apiClient.get('/genres/0')
    } catch (err: any) {
      expect(err.response.status).toBe(500)
    }
  })
})
