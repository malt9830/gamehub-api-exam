# Gamehub API

An [Express](https://expressjs.com/) app using [MongoDB](https://www.mongodb.com/) for the database and [Mongoose](https://mongoosejs.com/) to model objects, as well as [Nginx](https://www.nginx.com/) as a reverse-proxy.

## Docker Compose

### MongoDB

The *mongodb* service uses the [latest image](https://hub.docker.com/_/mongo), and binds the volume `mongodb-volume` to `/data/db` - as demonstrated in their documentation - in order for the data to persist.

As [recommended by Docker](https://docs.docker.com/config/containers/start-containers-automatically/), the `restart: always` will restart the container whenever it is stopped, unless it was done manually in which case it will restart when either the Docker daemon or the container itself is started again.

The porting of `27018:27017` is based on the local MongoDB server running on *27017*, hence simply running it on the incremented port of *27018*.

### App

The *app* service builds its image from the [`Dockerfile`](/Dockerfile) based on the [`node:20-slim`](https://hub.docker.com/layers/library/node/20-slim/images/sha256-80c3e9753fed11eee3021b96497ba95fe15e5a1dfc16aaf5bc66025f369e00dd) using version 20 with the *slim* affix to contain only the minimal packages need to run Node. The working directory is set to `/app/` into which `package.json` and `package-lock.json` are copied followed by running `npm ci` to install the exact dependencies in `package-lock`.

After having cleanly installed the dependencies we copy the the remaining project into our working directory, as doing this after the `npm ci` will save potential time due to layer caching.

`EXPOSE 3000` does not publish the port but simply serves as a type of documentation to describe the port listened to durign runtime.

Eventually the `start` script from `package.json` is run to start the application.

Additionally the `app` service depends on the `mongodb` service and declares environment variables of `MONGO_URI=mongodb://mongodb:27017/gamehub`, where the database's service name replaces the usual `localhost` or `127.0.0.1`, i.e. `mongodb:27017`.

### Nginx

The `nginx` service is using the [latest image](https://hub.docker.com/_/nginx) and the porting of `4000:4000`, while in return being dependent on the `app` service, thus creating a chain dependency to the `mongodb` service containing the `restart: always` option.

We bind the volume `./nginx/` to  `/etc/nginx/conf.d:ro` whereof `ro` indicates `read-only` permissions.

`nginx.conf` defines firstly an upstream block `gamehub-upstream` defines a server to which requests will be forwarded, this being the `3000` port our app is running on.

The second block, `server`, defines the server configurations, therein the port of `4000` on which Nginx will listen with the `server_name` being `local_host`.

Thereafter the sub-block `location` matches any routes with `/` indicating requests made to any route should be proxied to the `gamehub-upstream`, thus creating a reverse proxy on port `4000` forwarding requests to `3000`.