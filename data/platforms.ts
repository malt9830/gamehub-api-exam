/**
 * Array of sample platforms.
 * @type {Object[]}
 */
export default [
  {
    _id: '65ea12ecb4217bdb091a6259',
    name: 'PC',
    slug: 'pc',
  },
  {
    _id: '65ea13d2b4217bdb091a625c',
    name: 'PlayStation',
    slug: 'playstation',
  },
  {
    _id: '65ea13f8b4217bdb091a625d',
    name: 'XBox',
    slug: 'xbox',
  },
]
