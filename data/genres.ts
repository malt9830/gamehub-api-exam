/**
 * Array of sample genres.
 * @type {Object[]}
 */
export default [
  {
    _id: '65ea141ab4217bdb091a625e',
    name: 'Action',
    slug: 'action',
  },
  {
    _id: '65ea1435b4217bdb091a625f',
    name: 'Adventure',
    slug: 'adventure',
  },
]
