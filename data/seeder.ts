import mongoose from 'mongoose'
import { Game, Genre, Platform } from '../models/'
import { games, genres, platforms } from './index'

mongoose
  .connect('mongodb://localhost:27018/gamehub')
  .then(async () => {
    console.log('Connected to the database')

    await mongoose.connection.db.dropDatabase()
    console.log('Deleted all data')

    const savedGames = await Game.insertMany(games)
    const savedGenres = await Genre.insertMany(genres)
    const savedPlatforms = await Platform.insertMany(platforms)

    console.log(savedGames, savedGenres, savedPlatforms)
    console.log('Data seeded')
  })
  .catch((err) => {
    console.error('Error connecting to the database', err)
    process.exit(1)
  })
