/**
 * Array of sample games.
 * @type {Object[]}
 */
export default [
  {
    _id: '65ea2155ef284d7cf35a315a',
    name: 'Grand Theft Auto V',
    background_image: 'https://media.rawg.io/media/games/20a/20aa03a10cda45239fe22d035c0ebe64.jpg',
    metacritic: 90,
    genres: [{ _id: '65ea141ab4217bdb091a625e', name: 'Action', slug: 'action' }],
    platforms: [
      { _id: '65ea12ecb4217bdb091a6259', name: 'PC', slug: 'pc' },
      { _id: '65ea13d2b4217bdb091a625c', name: 'PlayStation', slug: 'playstation' },
      { _id: '65ea13f8b4217bdb091a625d', name: 'XBox', slug: 'xbox' },
    ],
  },
  {
    _id: '65ea240fef284d7cf35a3162',
    name: 'The Witcher 3: Wild Hunt',
    background_image: 'https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg',
    metacritic: 90,
    genres: [
      { _id: '65ea141ab4217bdb091a625e', name: 'Action', slug: 'action' },
      { _id: '65ea1435b4217bdb091a625f', name: 'Adventure', slug: 'adventure' },
    ],
    platforms: [
      { _id: '65ea12ecb4217bdb091a6259', name: 'PC', slug: 'pc' },
      { _id: '65ea13d2b4217bdb091a625c', name: 'PlayStation', slug: 'playstation' },
      { _id: '65ea13f8b4217bdb091a625d', name: 'XBox', slug: 'xbox' },
    ],
  },
  {
    _id: '65ea24adef284d7cf35a3168',
    name: 'Portal 2',
    background_image: 'https://media.rawg.io/media/games/2ba/2bac0e87cf45e5b508f227d281c9252a.jpg',
    metacritic: 90,
    genres: [{ _id: '65ea141ab4217bdb091a625e', name: 'Action', slug: 'action' }],
    platforms: [
      { _id: '65ea12ecb4217bdb091a6259', name: 'PC', slug: 'pc' },
      { _id: '65ea13d2b4217bdb091a625c', name: 'PlayStation', slug: 'playstation' },
    ],
  },
  {
    _id: '65ea250bef284d7cf35a3169',
    name: 'Counter-Strike: Global Offensive',
    background_image: 'https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg',
    metacritic: 90,
    genres: [{ _id: '65ea141ab4217bdb091a625e', name: 'Action', slug: 'action' }],
    platforms: [
      { _id: '65ea12ecb4217bdb091a6259', name: 'PC', slug: 'pc' },
      { _id: '65ea13d2b4217bdb091a625c', name: 'PlayStation', slug: 'playstation' },
      { _id: '65ea13f8b4217bdb091a625d', name: 'XBox', slug: 'xbox' },
    ],
  },
]
