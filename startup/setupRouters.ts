import express from 'express'
import platformRouter from '../routers/platformRouter'
import genreRouter from '../routers/genreRouter'
import gameRouter from '../routers/gameRouter'

/**
 * Sets up the /games, /genres and /platforms routes for the passed app.
 * @param {express.Application} app - an express.js app
 */
export default function setupRouters(app: express.Application) {
  app.use('/platforms', platformRouter)
  app.use('/genres', genreRouter)
  app.use('/games', gameRouter)
}
