import mongoose from 'mongoose'

/**
 * Connects to the MongoDB instance based on the MONGO_URI environment variable.
 * Throws an error if no MONGO_URI.
 * Console logs once connected if succesful.
 */
export default () => {
  if (!process.env.MONGO_URI) {
    throw new Error('MONGO_URI is not defined')
  }

  mongoose
    .connect(process.env.MONGO_URI)
    .then(() => console.log('Connected to MongoDB'))
    .catch((error) => console.error(error))
}
