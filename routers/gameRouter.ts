import express from 'express'
import Game, { IGame } from '../models/game'

const gameRouter = express.Router()

interface Response {
  count: number
  results: IGame[]
}

interface Query {
  'genre._id'?: string
  'platform._id'?: string
}

gameRouter.get('/', async (req, res) => {
  try {
    const query: Query = {}
    const genreID = req.query['genre']
    const platformID = req.query['platform']

    if (genreID) {
      query['genre._id'] = genreID as string
    }

    if (platformID) {
      query['platform._id'] = platformID as string
    }

    const games = await Game.find(query)
    const response: Response = {
      count: games.length,
      results: [...games],
    }

    res.send(response)
  } catch (err) {
    res.status(500).send(err)
  }
})

gameRouter.get('/:id', async (req, res) => {
  try {
    const game = await Game.findById(req.params.id)

    if (!game) {
      return res.status(404).send()
    }

    res.send(game)
  } catch (err) {
    res.status(500).send(err)
  }
})

export default gameRouter
